import { Avatar, Card, CardContent, CardHeader, CardMedia, Typography } from '@mui/material';
import { blue, red, yellow,lime  } from '@mui/material/colors';
import React, {useState} from 'react';
import PropTypes from "prop-types";

const Kartu = ({title, date, img, desc})=>{
    const [color, setColor]= useState(process.env.REACT_APP_COLOR_PRIMARY);
    
    function createTheme(change){
        if(change === "alpha"){
            return blue[200];
        }
        else if(change === "beta"){
            return red[200];
        }
        else if(change === "local"){
            return yellow[200];
        }else{
            return lime;
        }
    }
    return (
        <Card sx={{ maxWidth: 400, bgcolor:createTheme(color) }}>
            <CardHeader
                avatar={
                <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                    R
                </Avatar>
                }
                title={title}
                subheader={date}
            />
            <CardMedia
                component="img"
                height="400"
                image={img}
                alt="Paella dish"
            />
            <CardContent>
                <Typography variant="body2" color="text.secondary" sx={{fontFamily:'Helvetica'}} >
                {desc}
                </Typography>
            </CardContent>
        </Card>
    )
}

Kartu.propTypes = {
    title: PropTypes.string,
    date : PropTypes.string,
    img: PropTypes.string,
    desc: PropTypes.string
}
export default Kartu;
