import { BeachAccess, Image, Work } from "@mui/icons-material";
import { Avatar, Card, CardContent, CardHeader, CardMedia, Grid, List, ListItem, ListItemAvatar, ListItemText, Typography } from "@mui/material";
import { red } from '@mui/material/colors';
import axios from 'axios';
import React, { useEffect, useState, useCallback  } from 'react'
import Kartu from "../components/Kartu";

export const Homepage = () => {
    const [windowSize, setWindowSize] = useState(window.innerWidth);
    const apiURL = process.env.REACT_APP_API_URL;
    
    console.log(apiURL)
    const [Pink, setPink] = useState(8)
    const [Brown, setBrown] = useState(4)
    const [Data, setData] = useState([]);
    
    
    const fetchdata = ()=>{
        try {
            const api = {
            method : "get",
            url: "http://localhost:3004/postgenerated",
            headers : {},
            };
            axios(api).then((value)=>{
            setData(value.data);
            // console.log(value.data);
            });
            
            
        } catch (error) {
            console.log("Error nihh");
        }
        
    }

    console.log(windowSize);

    useEffect(()=>{
        fetchdata();
        const handleresize = ()=>{
            setWindowSize(window.innerWidth);
        };
        window.addEventListener('resize',handleresize);
        if(windowSize<900){
            setPink(12)
            setBrown(0)
        }
        else{
            setPink(8)
            setBrown(4)
        }
    },[windowSize,Pink,Brown]);
    // console.log(Data);
    const myFunction = () => {
      return new Promise((resolve, reject) => {
        // console.log(Data);
        Data.length == 0
        ?reject("Oh no Error Coba Cek Kembali URL/Jaringan pada Mockoon 😞")
        : resolve("Berhasil Connect API 🙌");
      });
    };
    // Hook
    const useAsync = (asyncFunction, immediate = true) => {
      const [status, setStatus] = useState("idle");
      const [value, setValue] = useState(null);
      const [error, setError] = useState(null);
      // The execute function wraps asyncFunction and
      // handles setting state for pending, value, and error.
      // useCallback ensures the below useEffect is not called
      // on every render, but only if asyncFunction changes.
      const execute = useCallback(() => {
        setStatus("pending");
        setValue(null);
        setError(null);
        return asyncFunction()
          .then((response) => {
            setValue(response);
            setStatus("success");
          })
          .catch((error) => {
            setError(error);
            setStatus("error");
          });
      }, [asyncFunction]);
      // Call execute if we want to fire it right away.
      // Otherwise execute can be called later, such as
      // in an onClick handler.
      useEffect(() => {
        if (immediate) {
          execute();
        }
      }, [execute, immediate]);
      return { execute, status, value, error };
    }
    const { execute, status, value, error } = useAsync(myFunction, false);
    
  return (
    <div>
      {status === "idle" && <div>Klik Tombol ini untuk Cek Koneksi API</div>}
      {status === "success" && <div>{value}</div>}
      {status === "error" && <div>{error}</div>}
      <button onClick={execute} disabled={status === "pending"}>
        {status !== "pending" ? "Click me" : "Loading..."}
      </button>
        <Grid container>
            <Grid item xs={Pink}>
                <Grid container >
                    {Data.map((item)=>{
                        return(
                            <Grid mt={2} item xl={6} sx={{justifyContent:'center'}}>
                                <Kartu title={item.title} date={item.datePost} img={item.img} desc={item.description}/>
                            </Grid>
                        )
                    })}
                    
                </Grid>
            
            </Grid>
            <Grid item xs={Brown} sx={{display: { xs: 'none', sm: 'block' }}}>
            <List sx={{ bgcolor: 'background.paper' }}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <Image/>
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Photos" secondary="Jan 9, 2014" />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <Work />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Work" secondary="Jan 7, 2014" />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <BeachAccess />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Vacation" secondary="July 20, 2014" />
            </ListItem>
          </List>
            </Grid>
        </Grid>
    </div>
  )
}
